const express=require('express');
const mongoose=require('mongoose');
require('dotenv').config();
const app=express();

app.use(express.json())


//all router
const quizRouter=require('./src/routes/user');
const studentRouter=require('./src/routes/student')

// mongo db connection
const url = process.env.MONGO_URL

mongoose.connect(url,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useCreateIndex:true,
    useFindAndModify:true
})
.then(console.log("Server connected"))
.catch(err=>{
    console.log(err);
})


app.use('/quiz', quizRouter);
app.use('/student', studentRouter);

const port=process.env.PORT || 5000
app.get('/',(req,res)=>{
    res.send(`<h1> I am from ROOT </h1>`)
})
app.listen(port,console.log("Running On Port ",port));