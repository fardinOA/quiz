const {
    createStudent,submitResult,getResult

} = require('../controller/student');
const express = require('express');
const router = express.Router();

router.post('/create', createStudent);
router.post('/submit/:qid/:sid',submitResult);
router.get('/get-result/:id/:qname',getResult);


module.exports = router;