const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const quizSchema= new Schema({
    className:{
        type:String,
        required:true
    },
    subjectName:{
        type:String,
        required:true
    },
    quizName:String,
    questions:[
        {
            problemSatement:String,
           answers:[
               
           ],
           correctIndx:Number
        }
    ]
})




module.exports=mongoose.model('Quiz',quizSchema);