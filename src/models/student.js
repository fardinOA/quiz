const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentSchema = new Schema({
   name:String,
   results:{
       quiz:[
           {
               quizName:String,
               mark:Number
           }
       ]
   }

})




module.exports = mongoose.model('Student', studentSchema);