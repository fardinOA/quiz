const Quiz=require('../models/quiz');



const createQuiz=async(req,res)=>{
    try{
        const {className,subjectName,quizName,questions}=req.body;
        let quiz=new Quiz({
            className,
            subjectName,
            quizName,
            questions
        })
        let isSave=await quiz.save();
        if(isSave){
            res.json({
                message:'create quiz successfully'
            })
        }
        else {
            res.json({
                message:'can not create quiz'
            })
        }
    }
    catch(err){
        res.json({
            err
        })
    }
}



module.exports={
    createQuiz,
    
}