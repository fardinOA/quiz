const Student = require('../models/student');
const Quiz=require('../models/quiz')


const createStudent = async (req, res) => {
    try {
        const { name } = req.body;
        let student = new Student({
            name
        })
        let isSave = await student.save();
        if (isSave) {
            res.json({
                message: 'create student successfully'
            })
        }
        else {
            res.json({
                message: 'can not create student'
            })
        }

    }
    catch (err) {
        
        res.json({
            err
        })
    }
}

const submitResult = async (req, res) => {
    try {
        
      const q_id=req.params.qid;
      const s_id=req.params.sid;
      const answers=req.body;
      const quizName=answers[0].quizName;
      console.log(quizName);
      
      const isFind = await Quiz.findById({_id:q_id});
      if(isFind){
          let mark=0;
         
         for(var i=0;i<answers.length;i++){
             let find = isFind.questions[i].correctIndx;
             if (find == answers[i].answer)mark=mark+5;
         }
         await Student.findByIdAndUpdate(
             {_id:s_id},
             {
                 $set:{
                     
                     results:{
                         quiz: [{quizName,mark}]
                     }
                 }
             },
             {
                 multi: true
             }
         )
         return res.json({
             message:'we save your response'
         })
      }

    }
    catch (err) {
       console.log(err);
        res.json({
            err
        })
    }
}


const getResult=async(req,res)=>{
    try {
        const _id=req.params.id;
        const qname=req.params.qname;
        const student=await Student.findById({_id});
        if(student){
            
            let ind;
            for(var i=0;i<student.results.quiz.length;i++){
                if (student.results.quiz[i].quizName==qname){
                    ind=i;
                    break;
                }
            }
            res.json({
                message:'your result of quiz',
                TotalMark:student.results.quiz[ind].mark
            })
        }
    } catch (err) {
        console.log(err);
        res.json({
          err
        })
    }
}

module.exports = {
    createStudent,
    submitResult,
    getResult
}